# Abstract Structured Document

A Java library for providing a format independent representation of hierarchical/structured data. It is intended to be type safe, easy to manipulate/create, and convertible to/from multiple formats such as JSON/Yaml/XML/etc.