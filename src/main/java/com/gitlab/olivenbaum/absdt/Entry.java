package com.gitlab.olivenbaum.absdt;

public class Entry {
    private String key;
    private Document value;

    public Entry(String key, Document value) {
        this.key = key;
        this.value = value;
    }

    public String key() {
        return key;
    }

    public Document value() {
        return value;
    }
}
