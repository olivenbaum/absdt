package com.gitlab.olivenbaum.absdt;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sequence implements Document {

    private final Supplier<Stream<Document>> children;

    public Sequence(Supplier<Stream<Document>> children) {
        this.children = children;
    }

    public Sequence(Collection<Document> children) {
        this(children::stream);
    }

    public Sequence(Document... children) {
        this(Arrays.asList(children));
    }

    public <T> T accept(DocumentVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public Stream<Document> children() {
        return children.get();
    }

    @Override
    public boolean hasChildren() {
        return true;
    }

    @Override
    public String toString() {
        return "[" +
            children()
                .map(e -> e.toString())
                .collect(Collectors.joining(", ")) +
            "]";
    }
}
