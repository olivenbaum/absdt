package com.gitlab.olivenbaum.absdt;

public interface DocumentVisitor<T> {
    T visit(Dictionary dict);

    T visit(Sequence obj);

    T visit(Scalar obj);
}
