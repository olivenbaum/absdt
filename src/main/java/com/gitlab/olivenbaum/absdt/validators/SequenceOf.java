package com.gitlab.olivenbaum.absdt.validators;

import com.gitlab.olivenbaum.absdt.Document;

import java.util.function.Predicate;

public class SequenceOf implements Predicate<Document> {
    private Predicate<Document> predicate;

    public SequenceOf(Predicate<Document> predicate) {
        this.predicate = predicate;
    }

    @Override
    public boolean test(Document document) {
        return !document.isScalar()
            && !document.hasEntries()
            && contentsMatch(document);
    }

    private boolean contentsMatch(Document document) {
        return document
            .children()
            .allMatch(predicate);
    }

}
