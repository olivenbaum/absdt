package com.gitlab.olivenbaum.absdt.validators;

import com.gitlab.olivenbaum.absdt.Document;
import com.gitlab.olivenbaum.absdt.Scalar;

import java.util.function.Predicate;

public class ValidatorsFactories {

    private ValidatorsFactories(){
    }

    private static final Predicate<Document> INTEGER = d -> d instanceof Scalar.IntScalar;
    private static final Predicate<Document> FLOATING = d -> d instanceof Scalar.DoubleScalar;
    private static final Predicate<Document> LONG = d -> d instanceof Scalar.LongScalar;
    private static final Predicate<Document> STRING = d -> d instanceof Scalar.StringScalar;
    private static final Predicate<Document> ANYTHING = d -> true;
    private static final DictionaryOf ANY_DICT = new DictionaryOf(ANYTHING);
    private static final SequenceOf ANY_SEQ = new SequenceOf(ANYTHING);
    private static final Predicate<Document> NOT_EMPTY_SEQ = d -> d.hasChildren() && d.children().count() > 0;
    private static final Predicate<Document> NOT_EMPTY_DICT = d -> d.hasEntries() && d.entries().count() > 0;

    public static SequenceOf sequenceOf(Predicate<Document> predicate) {
        return new SequenceOf(predicate);
    }

    public static DictionaryOf dictionaryOf(Predicate<Document> predicate) {
        return new DictionaryOf(predicate);
    }

    public static DictionaryOf anyDict() {
        return ANY_DICT;
    }

    public static SequenceOf anySeq(){
        return ANY_SEQ;
    }

    public static Predicate<Document> notEmptySeq(){
        return NOT_EMPTY_SEQ;
    }

    public static Predicate<Document> notEmptyDict(){
        return NOT_EMPTY_DICT;
    }

    public static Predicate<Document> anyInt(){
        return INTEGER;
    }

    public static Predicate<Document> anyFloat(){
        return FLOATING;
    }

    public static Predicate<Document> anyLong(){
        return LONG;
    }

    public static Predicate<Document> anyString(){
        return STRING;
    }

    public static Predicate<Document> anything(){
        return ANYTHING;
    }

    public static EntryPredicate entryp(String name, Predicate<Document> predicate) {
        return new EntryPredicate(name, predicate);
    }

    public static Predicate<Document> intp(Predicate<Integer> predicate) {
        return doc -> doc.asInt()
            .filter(predicate)
            .isPresent();
    }

    public static Predicate<Document> longp(Predicate<Long> predicate) {
        return doc -> doc.asLong()
            .filter(predicate)
            .isPresent();
    }

    public static Predicate<Document> floatingp(Predicate<Double> predicate) {
        return doc -> doc.asDouble()
            .filter(predicate)
            .isPresent();
    }

    public static Predicate<Document> stringp(Predicate<String> predicate) {
        return doc -> doc.asString()
            .filter(predicate)
            .isPresent();
    }


}
