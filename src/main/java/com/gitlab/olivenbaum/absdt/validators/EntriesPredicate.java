package com.gitlab.olivenbaum.absdt.validators;

import com.gitlab.olivenbaum.absdt.Document;
import com.gitlab.olivenbaum.absdt.Entry;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EntriesPredicate implements Predicate<Document> {

    private final Map<String, EntryPredicate> predicates;

    public EntriesPredicate(List<EntryPredicate> preds) {
        this.predicates = preds
            .stream()
            .collect(
                Collectors.toMap(e -> e.name(), e -> e)
            );
    }

    public EntriesPredicate(EntryPredicate... predicates) {
        this(Arrays.asList(predicates));
    }

    @Override
    public boolean test(Document document) {
        return document
            .entries()
            .allMatch(this::predicateMatch);
    }

    private boolean predicateMatch(Entry entry) {
        return predicates.containsKey(entry.key()) &&
            predicates.get(entry.key()).test(entry);
    }
}
