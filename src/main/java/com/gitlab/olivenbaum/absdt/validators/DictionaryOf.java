package com.gitlab.olivenbaum.absdt.validators;

import com.gitlab.olivenbaum.absdt.Document;

import java.util.function.Predicate;


public class DictionaryOf implements Predicate<Document> {
    private Predicate<Document> predicate;

    public DictionaryOf(Predicate<Document> predicate) {
        this.predicate = predicate;
    }

    @Override
    public boolean test(Document document) {
        return !document.isScalar()
            && !document.hasChildren()
            && entriesMatch(document);
    }

    private boolean entriesMatch(Document document) {
        return document
            .entries()
            .allMatch(e -> predicate.test(e.value()));
    }

    public Predicate<Document> withEntries(EntryPredicate ... predicates){
        return this.and(new EntriesPredicate(predicates));
    }

}
