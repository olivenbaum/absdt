package com.gitlab.olivenbaum.absdt.validators;

import com.gitlab.olivenbaum.absdt.Document;
import com.gitlab.olivenbaum.absdt.Entry;

import java.util.function.Predicate;

public class EntryPredicate implements Predicate<Entry> {
    private final String name;
    private Predicate<Document> predicate;

    public EntryPredicate(String name, Predicate<Document> predicate) {
        this.name = name;
        this.predicate = predicate;
    }

    @Override
    public boolean test(Entry entry) {
        return entry.key().equals(name) && predicate.test(entry.value());
    }

    public String name() {
        return name;
    }
}
