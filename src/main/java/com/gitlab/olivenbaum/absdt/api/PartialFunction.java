package com.gitlab.olivenbaum.absdt.api;

import java.util.Optional;

@FunctionalInterface
public interface PartialFunction<T, V> {
    Optional<V> apply(T value);
}
