package com.gitlab.olivenbaum.absdt.api;

import com.gitlab.olivenbaum.absdt.Dictionary;
import com.gitlab.olivenbaum.absdt.Document;

import static com.gitlab.olivenbaum.absdt.Factories.*;
import static com.gitlab.olivenbaum.absdt.api.Functional.*;
import static com.gitlab.olivenbaum.absdt.converters.Converters.integers;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class Partial<T,V> implements PartialFunction<T, V>{
    private final Function<T, Optional<V>> function;

    public Partial(Function<T, Optional<V>> function) {
        this.function = function;
    }

    @Override
    public Optional<V> apply(T value) {
        return function.apply(value);
    }

    public static void main(String[] args) {
        Dictionary dict = dictionary(
            entry("A", integer(1)),
            entry("B", integer(1)),
            entry("C", floating(1.0)),
            entry("D", integer(1))
        );

        Map<String, Integer> map = dict.asMapOf(integers());

        System.out.println(map);
    }
}
