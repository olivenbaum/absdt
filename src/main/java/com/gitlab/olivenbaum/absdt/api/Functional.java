package com.gitlab.olivenbaum.absdt.api;

import java.util.Optional;
import java.util.function.Function;

public class Functional {

    public static <T, V> Partial<T,V> partial(Function<T, Optional<V>> function){
        return new Partial<>(function);
    }
}
