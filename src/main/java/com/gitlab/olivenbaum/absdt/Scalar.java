package com.gitlab.olivenbaum.absdt;

import java.util.Optional;

public abstract class Scalar implements Document {

    public <T> T accept(DocumentVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean isScalar() {
        return true;
    }

    public static class IntScalar extends Scalar {
        private final int value;

        public IntScalar(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public Optional<Integer> asInt() {
            return Optional.of(value);
        }

        @Override
        public Optional<Long> asLong() {
            return Optional.of((long) value);
        }

        @Override
        public Optional<Double> asDouble() {
            return Optional.of((double) value);
        }
    }

    public static class DoubleScalar extends Scalar {
        private final double value;

        public DoubleScalar(double value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public Optional<Double> asDouble() {
            return Optional.of(value);
        }
    }

    public static class StringScalar extends Scalar {
        private final String value;

        public StringScalar(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "\"" + value + "\"";
        }

        @Override
        public Optional<String> asString() {
            return Optional.of(value);
        }
    }

    public static class LongScalar extends Scalar {
        private final long value;

        public LongScalar(long value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public Optional<Integer> asInt() {
            try {
                return Optional.of(Math.toIntExact(value));
            } catch (Exception ex) {
                return Optional.empty();
            }
        }

        @Override
        public Optional<Long> asLong() {
            return Optional.of(value);
        }

        @Override
        public Optional<Double> asDouble() {
            return Optional.of((double) value);
        }
    }

    public static class BoolScalar extends Scalar {
        private final boolean value;

        public BoolScalar(boolean value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public Optional<Boolean> asBool() {
            return Optional.of(value);
        }
    }

}
