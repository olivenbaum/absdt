package com.gitlab.olivenbaum.absdt.converters;

import com.gitlab.olivenbaum.absdt.Entry;
import org.json.JSONObject;

import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

class JSONDictionary implements Supplier<Stream<Entry>> {

    private final JSONObject object;

    public JSONDictionary(JSONObject object) {
        this.object = object;
    }

    @Override
    public Stream<Entry> get() {
        Iterable<String> iterable = () -> object.keys();
        Stream<String> keys = StreamSupport
            .stream(
                iterable.spliterator(),
                false
            );

        return keys.map(
            key -> new Entry(key, JSONConverter.convertJSONElement(object.get(key)))
        );
    }

}
