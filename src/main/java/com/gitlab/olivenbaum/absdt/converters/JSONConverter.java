package com.gitlab.olivenbaum.absdt.converters;

import com.gitlab.olivenbaum.absdt.Dictionary;
import com.gitlab.olivenbaum.absdt.Document;
import com.gitlab.olivenbaum.absdt.Factories;
import com.gitlab.olivenbaum.absdt.Sequence;
import org.json.JSONArray;
import org.json.JSONObject;


public class JSONConverter {

    private JSONConverter(){}

    public static Document convert(JSONArray jar){
        return convertJSONElement(jar);
    }

    public static Document convert(JSONObject job){
        return convertJSONElement(job);
    }


    static Document convertJSONElement(Object object) {
        if (object instanceof Integer) {
            return Factories.integer((Integer) object);
        } else if (object instanceof Double) {
            return Factories.floating((Double) object);
        } else if (object instanceof String) {
            return Factories.string((String) object);
        } else if (object instanceof Long) {
            return Factories.longint((Long) object);
        } else if (object instanceof Boolean) {
            return Factories.bool((Boolean) object);
        } else if (object instanceof JSONObject) {
            return new Dictionary(new JSONDictionary((JSONObject) object));
        } else {//if (object instanceof JSONArray) {
            return new Sequence(new JSONSequence((JSONArray) object));
        }

        //throw new RuntimeException("Impossible to convert.");
    }
}