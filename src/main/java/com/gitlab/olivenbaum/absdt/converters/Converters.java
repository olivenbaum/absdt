package com.gitlab.olivenbaum.absdt.converters;

import com.gitlab.olivenbaum.absdt.Document;
import com.gitlab.olivenbaum.absdt.api.Partial;

public class Converters {
    private static Partial<Document, Integer> INTEGERS = new Partial<>(Document::asInt);
    private static Partial<Document, Long> LONGS = new Partial<>(Document::asLong);
    private static Partial<Document, Double> FLOATS = new Partial<>(Document::asDouble);
    private static Partial<Document, Boolean> BOOLEANS = new Partial<>(Document::asBool);

    public static Partial<Document, Integer> integers(){
        return INTEGERS;
    }

    public static Partial<Document, Long> longs() {
        return LONGS;
    }

    public static Partial<Document, Double> floats(){
        return FLOATS;
    }

    public static Partial<Document, Boolean> booleans(){
        return BOOLEANS;
    }

}
