package com.gitlab.olivenbaum.absdt.converters;

import com.gitlab.olivenbaum.absdt.Document;
import org.json.JSONArray;

import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

class JSONSequence implements Supplier<Stream<Document>> {

    private final JSONArray array;

    public JSONSequence(JSONArray array) {
        this.array = array;
    }

    @Override
    public Stream<Document> get() {
        Iterable<Object> iterable = () -> array.iterator();
        Stream<Object> keys = StreamSupport
            .stream(iterable.spliterator(), false);

        return keys.map(JSONConverter::convertJSONElement);
    }
}
