package com.gitlab.olivenbaum.absdt.formatters;

import com.gitlab.olivenbaum.absdt.*;

import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;

public class JSONFormatter implements Function<Document, String> {

    private boolean prettyPrint;
    private int spaces;
    private JSONVisitor visitor;

    public JSONFormatter(boolean prettyPrint, int spaces) {
        this.prettyPrint = prettyPrint;
        this.spaces = spaces;
        this.visitor = new JSONVisitor();
    }

    public JSONFormatter() {
        this(true, 2);
    }

    public JSONFormatter(boolean prettyPrint) {
        this(prettyPrint, 2);
    }

    @Override
    public String apply(Document document) {
        return document.accept(visitor);
    }

    private class JSONVisitor implements DocumentVisitor<String> {
        private int indent = 0;

        @Override
        public String visit(Dictionary dict) {
            String str = "{" + delimiter();
            indent++;
            str += dict.entries()
                .map(e -> spaces() + "\"" + e.key() + "\":" + e.value().accept(this))
                .collect(Collectors.joining(", " + delimiter()));
            indent--;
            return str + delimiter() + spaces() + "}";
        }

        @Override
        public String visit(Sequence seq) {
            String str = "[" + delimiter();
            indent++;
            str += seq.children()
                .map(e -> spaces() + e.accept(this))
                .collect(Collectors.joining(", " + delimiter()));
            indent--;
            return str + delimiter() + spaces() + "]";
        }

        @Override
        public String visit(Scalar obj) {
            return obj.toString();
        }

        private String spaces() {
            return prettyPrint ?
                String.join("", Collections.nCopies(indent * spaces, " ")) :
                "";
        }

        private String delimiter() {
            return prettyPrint ? "\n" : "";
        }
    }
}