package com.gitlab.olivenbaum.absdt.formatters;

import com.gitlab.olivenbaum.absdt.*;
import com.gitlab.olivenbaum.absdt.converters.JSONConverter;
import org.json.JSONObject;

import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;

public class YamlFormatter implements Function<Document, String> {

    private final int spaces;
    private final DocumentVisitor<String> visitor;

    public YamlFormatter(int spaces) {
        this.spaces = spaces;
        this.visitor = new YamlVisitor();
    }

    public YamlFormatter() {
        this(2);
    }

    @Override
    public String apply(Document document) {
        return document.accept(visitor);
    }

    public class YamlVisitor implements DocumentVisitor<String> {
        private int indent = -1;

        @Override
        public String visit(Dictionary dict) {
            String str = "";
            indent++;

            str += dict
                .entries()
                .findFirst()
                .map(e -> e.key()
                    + ": "
                    + ( ! e.value().isScalar() ? "\n" + spaces(indent + 1) : "")
                    + e.value().accept(this) + "\n")
                .orElse("");

            str += dict.entries()
                .skip(1)
                .map(e -> spaces()
                    + e.key()
                    + ": "
                    + ( ! e.value().isScalar() ? "\n" + spaces(indent + 1) : "")
                    + e.value().accept(this))
                .collect(Collectors.joining("\n"));
            indent--;
            return str;// + "\n" + spaces();
        }

        @Override
        public String visit(Sequence seq) {
            String str = "";
            indent++;

            str += seq
                .children()
                .findFirst()
                .map(e -> "- " + e.accept(this) + "\n")
                .orElse("");

            str += seq.children()
                .skip(1)
                .map(e -> spaces() + "- " + e.accept(this))
                .collect(Collectors.joining("\n"));
            indent--;
            return str;
        }

        @Override
        public String visit(Scalar obj) {
            return obj.toString().replace("\"", "'");
        }

        private String spaces() {
            return String.join("", Collections.nCopies(indent * spaces, " "));
        }

        private String spaces(int ind) {
            return String.join("", Collections.nCopies(ind * spaces, " "));
        }
    }

    public static void main(String[] args) {
        String json = "{\n" +
            "  \"a\":1, \n" +
            "  \"b\":2, \n" +
            "  \"c\":34359738368, \n" +
            "  \"strawberry\":[\n" +
            "    10, \n" +
            "    true, \n" +
            "    1, \n" +
            "    [\n" +
            "      1, \n" +
            "      2, \n" +
            "      -4\n" +
            "    ]\n" +
            "  ]\n" +
            "}";

        String json2 = "{\n" +
            "  \"firstName\": \"John\",\n" +
            "  \"lastName\": \"Doe\",\n" +
            "  \"age\": 31,\n" +
            "  \"contactDetails\": [\n" +
            "    {\n" +
            "      \"type\": \"mobile\",\n" +
            "      \"number\": 123456789\n" +
            "    },\n" +
            "    {\n" +
            "      \"type\": \"landline\",\n" +
            "      \"number\": 456786868\n" +
            "    }\n" +
            "  ],\n" +
            "  \"homeAddress\": {\n" +
            "    \"line\": \"Xyz, DEF Street\",\n" +
            "    \"city\": \"City Y\",\n" +
            "    \"state\": \"State Y\",\n" +
            "    \"zip\": 345657\n" +
            "  }\n" +
            "}";

        Document doc = JSONConverter.convert(new JSONObject(json2));

        System.out.println(new YamlFormatter(2).apply(doc));
    }
}
