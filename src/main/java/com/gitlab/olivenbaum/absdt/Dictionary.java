package com.gitlab.olivenbaum.absdt;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Dictionary implements Document {
    private final Supplier<Stream<Entry>> entries;

    public Dictionary(Supplier<Stream<Entry>> entries) {
        this.entries = entries;
    }

    public Dictionary(Map<String, Document> entries) {
        this(() -> entries
            .entrySet()
            .stream()
            .map(e -> new Entry(e.getKey(), e.getValue()))
        );
    }

    public Dictionary(Collection<Entry> entries) {
        this(entries::stream);
    }

    public Dictionary(Entry... entries) {
        this(Arrays.asList(entries));
    }

    public <T> T accept(DocumentVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public boolean hasEntries() {
        return true;
    }

    @Override
    public Stream<Entry> entries() {
        return entries.get();
    }

    @Override
    public String toString() {
        return "{" +
            entries()
                .map(e -> e.key() + ": " + e.value())
                .collect(Collectors.joining(", ")) +
            "}";
    }
}
