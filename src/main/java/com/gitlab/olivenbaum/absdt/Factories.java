package com.gitlab.olivenbaum.absdt;

public class Factories {

    private Factories(){}

    public static Scalar integer(int value) {
        return new Scalar.IntScalar(value);
    }

    public static Scalar floating(double value) {
        return new Scalar.DoubleScalar(value);
    }

    public static Scalar string(String value) {
        return new Scalar.StringScalar(value);
    }

    public static Scalar longint(long value) {
        return new Scalar.LongScalar(value);
    }

    public static Scalar bool(boolean value) {
        return new Scalar.BoolScalar(value);
    }

    public static Sequence sequence(Document... children) {
        return new Sequence(children);
    }

    public static Dictionary dictionary(Entry... entries) {
        return new Dictionary(entries);
    }

    public static Entry entry(String key, int value) {
        return new Entry(key, integer(value));
    }

    public static Entry entry(String key, double value) {
        return new Entry(key, floating(value));
    }

    public static Entry entry(String key, String value) {
        return new Entry(key, string(value));
    }

    public static Entry entry(String key, long value) {
        return new Entry(key, longint(value));
    }

    public static Entry entry(String key, boolean value) {
        return new Entry(key, bool(value));
    }

    public static Entry entry(String key, Document value) {
        return new Entry(key, value);
    }
}
