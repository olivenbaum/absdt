package com.gitlab.olivenbaum.absdt;

import com.gitlab.olivenbaum.absdt.api.Pair;
import com.gitlab.olivenbaum.absdt.api.PartialFunction;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Document {
    default Stream<Document> children() {
        return Stream.of();
    }

    default Stream<Entry> entries() {
        return Stream.of();
    }

    default boolean hasChildren() {
        return false;
    }

    default boolean hasEntries() {
        return false;
    }

    default boolean isScalar() {
        return false;
    }

    default Map<String, Document> asMap() {
        return entries().
            collect(
                Collectors.toMap(
                    e -> e.key(),
                    e -> e.value()
                )
            );
    }

    default <T> Map<String, T> asMapOf(Function<Document, T> function){
        return entries()
            .map(e -> new Pair<>(e.key(), function.apply(e.value()) ))
            .collect(
                Collectors.toMap(
                    p -> p.first,
                    p -> p.second
                )
            );
    }

    default <T> Map<String, T> asMapOf(PartialFunction<Document, T> partial){
        return entries()
            .map(e -> new Pair<>(e.key(), partial.apply(e.value()) ))
            .filter( p -> p.second.isPresent() )
            .collect(
                Collectors.toMap(
                    p -> p.first,
                    p -> p.second.get()
                )
            );
    }

    default List<Document> asList() {
        return children().collect(Collectors.toList());
    }

    default <T> List<T> asListOf(Function<Document, T> function) {
        return children()
            .map( function )
            .collect(Collectors.toList());
    }

    default <T> List<T> asListOf(PartialFunction<Document, T> function) {
        return children()
            .map( d -> function.apply(d) )
            .filter( Optional::isPresent )
            .map( Optional::get )
            .collect(Collectors.toList());
    }

    <T> T accept(DocumentVisitor<T> visitor);

    default Optional<Long> asLong() {
        return Optional.empty();
    }

    default Optional<String> asString() {
        return Optional.of(this.toString());
    }

    default Optional<Integer> asInt() {
        return Optional.empty();
    }

    default Optional<Double> asDouble() {
        return Optional.empty();
    }

    default Optional<Boolean> asBool() {
        return Optional.empty();
    }
}
