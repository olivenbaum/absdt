package com.gitlab.olivenbaum.absdt;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.gitlab.olivenbaum.absdt.Factories.dictionary;
import static com.gitlab.olivenbaum.absdt.Factories.entry;

public class DictionaryTest {

    @Test
    public void test_dictionary_from_supplier() {
        Map<String, Integer> map = new TreeMap<String, Integer>() {{
            put("xxx", 10);
            put("yyy", 20);
            put("zzz", 30);
        }};

        Dictionary dict = new Dictionary(() -> Stream.of(
            new Entry("xxx", new Scalar.IntScalar(10)),
            new Entry("yyy", new Scalar.IntScalar(20)),
            new Entry("zzz", new Scalar.IntScalar(30))
        ));

        Assert.assertEquals(
            map,
            toIntMap(dict)
        );
    }

    @Test
    public void test_dictionary_from_map() {
        Map<String, Integer> map1 = new TreeMap<String, Integer>() {{
            put("xxx", 10);
            put("yyy", 20);
            put("zzz", 30);
        }};

        Map<String, Document> map2 = new HashMap<String, Document>() {{
            put("xxx", new Scalar.IntScalar(10));
            put("yyy", new Scalar.IntScalar(20));
            put("zzz", new Scalar.IntScalar(30));
        }};

        Dictionary dict = new Dictionary(map2);

        Assert.assertEquals(
            map1,
            toIntMap(dict)
        );
    }

    @Test
    public void test_dictionary_from_collection() {
        Map<String, Integer> map1 = new TreeMap<String, Integer>() {{
            put("xxx", 10);
            put("yyy", 20);
            put("zzz", 30);
        }};

        List<Entry> list = Arrays.asList(
            new Entry("xxx", new Scalar.IntScalar(10)),
            new Entry("yyy", new Scalar.IntScalar(20)),
            new Entry("zzz", new Scalar.IntScalar(30))
        );

        Dictionary dict = new Dictionary(list);

        Assert.assertEquals(
            map1,
            toIntMap(dict)
        );
    }

    @Test
    public void test_dictionary_from_variable_arguments() {
        Map<String, Integer> map1 = new TreeMap<String, Integer>() {{
            put("xxx", 10);
            put("yyy", 20);
            put("zzz", 30);
        }};

        Dictionary dict = new Dictionary(
            new Entry("xxx", new Scalar.IntScalar(10)),
            new Entry("yyy", new Scalar.IntScalar(20)),
            new Entry("zzz", new Scalar.IntScalar(30))
        );

        Assert.assertEquals(
            map1,
            toIntMap(dict)
        );
    }

    @Test
    public void test_empty_dictionary() {
        Map<String, Integer> map1 = new TreeMap<>();

        Dictionary dict = new Dictionary();

        Assert.assertEquals(
            map1,
            toIntMap(dict)
        );
    }

    @Test
    public void test_to_string(){
        Document doc = dictionary(
            entry("a", 1),
            entry("b", 2),
            entry("c", 2.5)
        );

        Assert.assertEquals(doc.toString(), "{a: 1, b: 2, c: 2.5}");
    }

    private Map<String, Integer> toIntMap(Dictionary dict){
        return dict.entries().collect(
            Collectors.toMap(
                e -> e.key(),
                e -> e.value().asInt().get()
            )
        );
    }

}
