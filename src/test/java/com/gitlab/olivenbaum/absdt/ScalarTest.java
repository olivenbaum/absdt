package com.gitlab.olivenbaum.absdt;

import org.testng.Assert;
import org.testng.annotations.Test;
import static com.gitlab.olivenbaum.absdt.Factories.*;

import java.util.NoSuchElementException;

public class ScalarTest {

    @Test
    public static void int_scalar_test(){
        Document doc = new Scalar.IntScalar(10);

        Assert.assertEquals(
            Integer.valueOf(10),
            doc.asInt().get()
        );
    }

    @Test(expectedExceptions = {NoSuchElementException.class})
    public static void int_scalar_not_found(){
        Document doc = new Scalar.StringScalar("1");

        int i = doc.asInt().get();
    }

    @Test
    public static void long_scalar_test(){
        Document doc = new Scalar.LongScalar(68719476736l);

        Assert.assertEquals(
            Long.valueOf(68719476736l),
            doc.asLong().get()
        );
    }

    @Test
    public static void long_conversion_successful(){
        Document doc = new Scalar.LongScalar(10l);

        Assert.assertEquals(
            Integer.valueOf(10),
            doc.asInt().get()
        );
    }

    @Test(expectedExceptions = {NoSuchElementException.class})
    public static void long_conversion_not_successful(){
        Document doc = new Scalar.LongScalar(68719476736l);

        doc.asInt().get();
    }

    @Test
    public static void long_to_double_conversion(){
        Document doc = longint(10l);
        double val = doc.asDouble().get();

        Assert.assertEquals( val, 10.0 );
    }


}
