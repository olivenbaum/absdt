package com.gitlab.olivenbaum.absdt;

import com.gitlab.olivenbaum.absdt.formatters.YamlFormatter;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.yaml.snakeyaml.Yaml;

import java.util.Map;

import static com.gitlab.olivenbaum.absdt.Factories.*;

public class YamlFormatterTest {

    @Test
    public void test_formatting_01() {
        String yaml = "firstName: 'John'\n" +
            "lastName: 'Doe'\n" +
            "age: 31\n" +
            "contactDetails: \n" +
            "   - type: 'mobile'\n" +
            "     number: 123456789\n" +
            "   - type: 'landline'\n" +
            "     number: 456786868\n" +
            "homeAddress: \n" +
            "   line: 'Xyz, DEF Street'\n" +
            "   city: 'City Y'\n" +
            "   state: 'State Y'\n" +
            "   zip: 345657";

        Document doc = dictionary(
            entry("firstName", "John"),
            entry("lastName", "Doe"),
            entry("age", 31),
            entry("contactDetails", sequence(
                dictionary(
                    entry("type", "mobile"),
                    entry("number", 123456789)
                ),
                dictionary(
                    entry("type", "landline"),
                    entry("number", 456786868)
                )
            )),
            entry("homeAddress", dictionary(
                entry("line", "Xyz, DEF Street"),
                entry("city", "City Y"),
                entry("state", "State Y"),
                entry("zip", 345657)
            ))
        );

        Yaml y = new Yaml();

        Assert.assertEquals(
            (Map) y.load(yaml),
            (Map) y.load(new YamlFormatter(2).apply(doc))
        );
    }
}
