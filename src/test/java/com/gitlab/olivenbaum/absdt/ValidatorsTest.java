package com.gitlab.olivenbaum.absdt;

import static com.gitlab.olivenbaum.absdt.Factories.*;
import static com.gitlab.olivenbaum.absdt.validators.ValidatorsFactories.*;
import static org.testng.Assert.assertTrue;

import com.gitlab.olivenbaum.absdt.validators.ValidatorsFactories;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.print.Doc;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ValidatorsTest {

    @Test
    public void test_int_dict_validator(){
        Document d1 = dictionary(
            entry("x", integer(10)),
            entry("y", integer(20)),
            entry("z", integer(30)),
            entry("w", integer(40))
        );

        Predicate<Document> p1 = dictionaryOf(
            anyInt()
        );

        Predicate<Document> p2 = dictionaryOf(
            anyLong()
        );

        Predicate<Document> p3 = dictionaryOf(
            anyString()
        );

        Predicate<Document> p4 = sequenceOf(
            anyInt()
        );

        assertTrue(p1.test(d1));
        assertTrue(p1.test(d1)); //check if it is idempotent
        assertTrue(p1.test(d1)); //check if it is idempotent

        Assert.assertFalse(p2.test(d1));
        Assert.assertFalse(p3.test(d1));
        Assert.assertFalse(p4.test(d1));
    }

    @Test
    public void test_int_list_validator(){
        Document d1 = sequence(
            integer(10), integer(20), integer(30), integer(40)
        );

        Predicate<Document> p1 = sequenceOf(
            anyInt()
        );

        Predicate<Document> p2 = sequenceOf(
            anyLong()
        );

        Predicate<Document> p3 = sequenceOf(
            anyString()
        );

        Predicate<Document> p4 = dictionaryOf(
            anyInt()
        );

        assertTrue(p1.test(d1));
        assertTrue(p1.test(d1)); //check if it is idempotent
        assertTrue(p1.test(d1)); //check if it is idempotent

        Assert.assertFalse(p2.test(d1));
        Assert.assertFalse(p3.test(d1));
        Assert.assertFalse(p4.test(d1));
    }

    @Test
    public void test_empty_seq_validators(){
        Document d1 = sequence();

        Predicate<Document> p1 = notEmptySeq().and(sequenceOf(anyInt()));

        Predicate<Document> p2 = sequenceOf(anyInt());
        Predicate<Document> p3 = sequenceOf(anyString());
        Predicate<Document> p4 = sequenceOf(anyLong());
        Predicate<Document> p5 = sequenceOf(anything());

        Predicate<Document> p6 = dictionaryOf(anything());

        Assert.assertFalse(p1.test(d1));
        Assert.assertFalse(p6.test(d1));

        assertTrue(p2.test(d1));
        assertTrue(p3.test(d1));
        assertTrue(p4.test(d1));
        assertTrue(p5.test(d1));
    }

    @Test
    public void test_empty_dict_validators(){
        Document d1 = dictionary();

        Predicate<Document> p1 = notEmptyDict().and(dictionaryOf(anyInt()));

        Predicate<Document> p2 = dictionaryOf(anyInt());
        Predicate<Document> p3 = dictionaryOf(anyString());
        Predicate<Document> p4 = dictionaryOf(anyLong());
        Predicate<Document> p5 = dictionaryOf(anything());

        Predicate<Document> p6 = sequenceOf(anything());

        Assert.assertFalse(p1.test(d1));
        Assert.assertFalse(p6.test(d1));

        assertTrue(p2.test(d1));
        assertTrue(p3.test(d1));
        assertTrue(p4.test(d1));
        assertTrue(p5.test(d1));
    }

    @Test
    public void test_scalar_is_not_dict_or_seq(){
        Document d1 = new Scalar.IntScalar(10);

        Predicate<Document> p1 = notEmptyDict().and(dictionaryOf(anyInt()));
        Predicate<Document> p2 = dictionaryOf(anyInt());
        Predicate<Document> p3 = dictionaryOf(anyString());
        Predicate<Document> p4 = sequenceOf(anything());

        boolean b = Stream
            .of(p1,p2,p3,p4)
            .map( p -> p.test(d1))
            .reduce(false, (x,y) -> x || y)
            .booleanValue();

        Assert.assertFalse(b);
    }

    @Test
    public void test_not_empty_seq(){
        Document d1 = sequence(
            integer(1), integer(2), integer(3)
        );

        Document d2 = sequence();

        Predicate<Document> p1 = notEmptySeq().and( sequenceOf(anyInt()));
        Predicate<Document> p2 = sequenceOf(anyInt());
        Predicate<Document> p3 = anySeq().and(notEmptySeq());
        Predicate<Document> p4 = anySeq();

        assertTrue(p1.test(d1));
        Assert.assertFalse(p1.test(d2));

        assertTrue(p2.test(d1));
        assertTrue(p2.test(d2)); // it is true due the vacuous truth property

        assertTrue(p3.test(d1));
        Assert.assertFalse(p3.test(d2));

        assertTrue(p4.test(d1));
        assertTrue(p4.test(d2));
    }

    @Test
    public void test_not_empty_dict(){
        Document d1 = dictionary(
            entry("2", 1), entry("2", 2), entry("3", 3)
        );

        Document d2 = dictionary();

        Predicate<Document> p1 = notEmptyDict().and( dictionaryOf(anyInt()));
        Predicate<Document> p2 = dictionaryOf(anyInt());

        assertTrue(p1.test(d1));
        Assert.assertFalse(p1.test(d2));

        assertTrue(p2.test(d1));
        assertTrue(p2.test(d2)); // it is true due the vacuous truth property
    }

    @Test
    public void test_long_scalar_predicate(){
        Predicate<Document> longPred = longp( num -> num > 10l);

        assertTrue(longPred.test(new Scalar.LongScalar(11l)));
        Assert.assertFalse(longPred.test(new Scalar.LongScalar(10l)));
        Assert.assertFalse(longPred.test(new Scalar.LongScalar(9l)));
        Assert.assertFalse(longPred.test(new Scalar.DoubleScalar(12.0)));
    }

    @Test
    public void test_int_scalar_predicate(){
        Predicate<Document> intPred = intp( num -> num > 10);

        assertTrue(intPred.test(new Scalar.IntScalar(11)));
        Assert.assertFalse(intPred.test(new Scalar.IntScalar(10)));
        Assert.assertFalse(intPred.test(new Scalar.IntScalar(9)));
        Assert.assertFalse(intPred.test(new Scalar.DoubleScalar(12.0)));
    }

    @Test
    public void test_private_constructor_for_hyaku_pasento_coverage() throws Exception {
        Constructor constructor = ValidatorsFactories.class.getDeclaredConstructor();
        Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));

        constructor.setAccessible(true);
        constructor.newInstance();
    }
}
