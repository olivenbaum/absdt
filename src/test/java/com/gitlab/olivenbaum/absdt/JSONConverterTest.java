package com.gitlab.olivenbaum.absdt;

import com.gitlab.olivenbaum.absdt.converters.JSONConverter;
import com.gitlab.olivenbaum.absdt.formatters.JSONFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.print.Doc;

import static com.gitlab.olivenbaum.absdt.validators.ValidatorsFactories.*;
import static org.testng.AssertJUnit.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JSONConverterTest {

    @Test
    public void test_double_json_array_conversion(){
        JSONArray jar = new JSONArray("[1.0, 2.0, 3.0, 4.0]");
        Document doc = JSONConverter.convert(jar);

        Assert.assertEquals(
            Arrays.asList(1.0, 2.0, 3.0, 4.0),
            doc.children()
                .map(d -> d.asDouble().get())
                .collect(Collectors.toList())
        );
    }

    @Test
    public void test_long_json_array_conversion(){
        JSONArray jar = new JSONArray("[34359738368, 34359738369, 34359738370, 34359738371]");
        Document doc = JSONConverter.convert(jar);

        Assert.assertEquals(
            Arrays.asList(34359738368l, 34359738369l, 34359738370l, 34359738371l),
            doc.children()
                .map(d -> d.asLong().get())
                .collect(Collectors.toList())
        );
    }

    @Test
    public void test_json_object_conversion(){
        JSONArray jar = new JSONArray("[1.0, 2.0, 3.0, 4.0]");
        Document doc = JSONConverter.convert(jar);

        Assert.assertEquals(
            Arrays.asList(1.0, 2.0, 3.0, 4.0),
            doc.children()
                .map(d -> d.asDouble().get())
                .collect(Collectors.toList())
        );
    }

    @Test
    public void test_nested_json_1(){
        JSONArray jar = new JSONArray("[\n" +
            "  1.0,\n" +
            "  2.0,\n" +
            "  {\n" +
            "    \"a\": 10,\n" +
            "    \"b\": 20,\n" +
            "    \"c\": 30,\n" +
            "    \"d\": [\n" +
            "      \"a\",\n" +
            "      \"b\",\n" +
            "      \"c\"\n" +
            "    ]\n" +
            "  }\n" +
            "]");

        Predicate<Document> pred = sequenceOf(
            anyFloat().or(dictionaryOf(
                anyInt().or(sequenceOf(anyString()))
            ))
        );

        Document doc = JSONConverter.convert(jar);

        Assert.assertTrue(pred.test(doc));

        String docToString = new JSONFormatter(false).apply(doc);

        JSONArray jar2 = new JSONArray(docToString);

        Assert.assertEquals(jar.toString(), jar2.toString());
    }

    @Test
    public void test_nested_json_2(){
        String json = "{\n" +
            "  \"a\": 1,\n" +
            "  \"b\": 2,\n" +
            "  \"c\": 34359738368,\n" +
            "  \"strawberry\": [\n" +
            "    10,\n" +
            "    true,\n" +
            "    1,\n" +
            "    [\n" +
            "      1,\n" +
            "      2,\n" +
            "      -4\n" +
            "    ]\n" +
            "  ]\n" +
            "}";

        List list1 = new ArrayList();
        list1.add(10);
        list1.add(true);
        list1.add(null);
        list1.add(Arrays.asList(1, 2, -4));

        Map m1 = new HashMap();
        m1.put("a", 1);
        m1.put("b", 2);
        m1.put("c", 34359738368l);
        m1.put("strawberry", list1);

        Document d1 = JSONConverter.convert(new JSONObject(json));

        Map<String, Document> m2 = d1.asMap();

        Assert.assertEquals(
            m1.get("a"),
            m2.get("a").asInt().get()
        );

        Assert.assertEquals(
            m1.get("b"),
            m2.get("b").asInt().get()
        );

        Assert.assertEquals(
            m1.get("c"),
            m2.get("c").asLong().get()
        );

        String docToString = new JSONFormatter(false).apply(d1);

        JSONObject job = new JSONObject(docToString);

        Assert.assertEquals(new JSONObject(json).toString(), job.toString());
    }

    @Test
    public void test_non_pretty_printed_json(){
        String json = "{\n" +
            "  \"a\": 1,\n" +
            "  \"b\": 2,\n" +
            "  \"c\": 34359738368,\n" +
            "  \"strawberry\": [\n" +
            "    10,\n" +
            "    true,\n" +
            "    1,\n" +
            "    [\n" +
            "      1,\n" +
            "      2,\n" +
            "      -4\n" +
            "    ]\n" +
            "  ]\n" +
            "}";

        Assert.assertEquals(
            "{\"a\":1, \"b\":2, \"c\":34359738368, \"strawberry\":[10, true, 1, [1, 2, -4]]}",
            new JSONFormatter(false).apply(JSONConverter.convert(new JSONObject(json)))
        );
    }

    @Test
    public void test_pretty_printed_json(){
        String json = "{\n" +
            "  \"a\":1, \n" +
            "  \"b\":2, \n" +
            "  \"c\":34359738368, \n" +
            "  \"strawberry\":[\n" +
            "    10, \n" +
            "    true, \n" +
            "    1, \n" +
            "    [\n" +
            "      1, \n" +
            "      2, \n" +
            "      -4\n" +
            "    ]\n" +
            "  ]\n" +
            "}";

        Assert.assertEquals(
            json,
            new JSONFormatter().apply(JSONConverter.convert(new JSONObject(json)))
        );
    }

    @Test
    public void test_private_constructor_for_hyaku_pasento_coverage() throws Exception {
        Constructor constructor = JSONConverter.class.getDeclaredConstructor();
        assertTrue("Constructor is not private", Modifier.isPrivate(constructor.getModifiers()));

        constructor.setAccessible(true);
        constructor.newInstance();
    }
}
