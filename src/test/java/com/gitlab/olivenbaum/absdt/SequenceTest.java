package com.gitlab.olivenbaum.absdt;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SequenceTest {

    @Test
    public void test_sequence_from_supplier(){
        Supplier<Stream<Document>> sup = () -> Stream
            .of(1,2,3,4,5,6)
            .map( i -> new Scalar.IntScalar(i));

        List<Integer> list = Arrays.asList(
            1,2,3,4,5,6
        );

        Assert.assertEquals(
            list,
            toIntList(new Sequence(sup))
        );
    }

    @Test
    public void test_sequence_from_collection(){
        List<Document> list1 = Arrays.asList(
            new Scalar.IntScalar(1),
            new Scalar.IntScalar(2),
            new Scalar.IntScalar(3),
            new Scalar.IntScalar(4)
        );


        List<Integer> list2 = Arrays.asList(
            1,2,3,4
        );

        Assert.assertEquals(
            list2,
            toIntList(new Sequence(list1))
        );
    }

    @Test
    public void test_sequence_from_array(){
        List<Integer> list = Arrays.asList(
            1,2,3,4
        );

        Assert.assertEquals(
            list,
            toIntList(new Sequence(
                new Scalar.IntScalar(1),
                new Scalar.IntScalar(2),
                new Scalar.IntScalar(3),
                new Scalar.IntScalar(4)
            ))
        );
    }

    @Test
    public void test_empty_constructor(){
        List<Integer> list = new ArrayList<>();

        Assert.assertEquals(
            list,
            toIntList(new Sequence())
        );
    }

    @Test
    public void test_empty_supplier(){
        List<Integer> list = new ArrayList<>();

        Assert.assertEquals(
            list,
            toIntList(new Sequence(() -> Stream.of()))
        );
    }

    @Test
    public void test_toString(){
        Assert.assertEquals(
            "[1, 2, 3, 4]",
            new Sequence(
                new Scalar.IntScalar(1),
                new Scalar.IntScalar(2),
                new Scalar.IntScalar(3),
                new Scalar.IntScalar(4)
            ).toString()
        );
    }

    private List<Integer> toIntList(Sequence seq){
        return seq.asList()
            .stream()
            .map( i -> i.asInt().get() )
            .collect(Collectors.toList());
    }
}
