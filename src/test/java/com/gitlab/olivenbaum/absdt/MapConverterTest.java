package com.gitlab.olivenbaum.absdt;

import com.gitlab.olivenbaum.absdt.api.PartialFunction;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static com.gitlab.olivenbaum.absdt.converters.Converters.*;
import static com.gitlab.olivenbaum.absdt.Factories.*;
import static com.gitlab.olivenbaum.absdt.validators.ValidatorsFactories.*;

public class MapConverterTest {

    @Test
    public void test_list_of_ints(){
        Document doc = new Sequence(
            () -> IntStream.range(1, 10).mapToObj( Scalar.IntScalar::new )
        );

        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9);

        Assert.assertEquals(list, doc.asListOf(integers()));
    }

    @Test
    public void test_list_of_longs(){
        Document doc = new Sequence(
            () -> LongStream.range(1, 10).mapToObj( Scalar.LongScalar::new )
        );

        List<Long> list = Arrays.asList(1l,2l,3l,4l,5l,6l,7l,8l,9l);

        Assert.assertEquals(list, doc.asListOf(longs()));
    }


    @Test
    public void test_custom_converter(){
        class PointConverter implements PartialFunction<Document, Point> {
            private Predicate<Document> pred = anyDict()
                .withEntries(
                    entryp("x", anyInt()),
                    entryp("y", anyInt())
                );

            @Override
            public Optional<Point> apply(Document value) {
                if(!pred.test(value)){
                    return Optional.empty();
                }

                Map<String, Document> map = value.asMap();

                return Optional.of(new Point(
                    map.get("x").asInt().get(),
                    map.get("y").asInt().get()
                ));
            }
        }

        Document seq = sequence(
            dictionary( entry("x", 1), entry("y", 1)),
            dictionary( entry("x", 2), entry("y", 2)),
            dictionary( entry("x", 3), entry("y", 3)),
            dictionary( entry("x", 4), entry("y", 4)),
            dictionary( entry("x", 5), entry("y", 5)),
            integer(10),
            dictionary( entry("X", 6), entry("Y", 6))
        );

        List<Point> list = seq.asListOf(new PointConverter());

        Assert.assertEquals(
            list,
            Arrays.asList(
                new Point(1,1),
                new Point(2,2),
                new Point(3,3),
                new Point(4,4),
                new Point(5,5)
            )
        );
    }
}
