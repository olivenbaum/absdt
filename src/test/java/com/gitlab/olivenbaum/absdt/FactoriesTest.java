package com.gitlab.olivenbaum.absdt;

import org.testng.Assert;
import org.testng.annotations.Test;
import static com.gitlab.olivenbaum.absdt.Factories.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Map;

public class FactoriesTest {

    @Test
    public void test_boolean_dict(){
        Document doc = dictionary(
            entry("a", true),
            entry("b", false),
            entry("c", true)
        );

        Map<String, Document> map = doc.asMap();

        Assert.assertTrue(map.get("a").asBool().get() );
        Assert.assertFalse(map.get("b").asBool().get() );
        Assert.assertTrue(map.get("c").asBool().get() );
    }

    @Test
    public void test_string_dict(){
        Document doc = dictionary(
            entry("a", "AAAAAAAAAA"),
            entry("b", "BBBBbbBBBB"),
            entry("c", "CCCCCCCCCCCCCCCCCCCCCCCCCC")
        );

        Map<String, Document> map = doc.asMap();

        Assert.assertEquals(map.get("a").asString().get(), "AAAAAAAAAA" );
        Assert.assertEquals(map.get("b").asString().get(), "BBBBbbBBBB" );
        Assert.assertEquals(map.get("c").asString().get(), "CCCCCCCCCCCCCCCCCCCCCCCCCC" );
    }

    @Test
    public void test_long(){
        Document doc = dictionary(
            entry("a", 1099511627776l ),
            entry("b", 1099511627774l),
            entry("c", 1099511627779l)
        );

        Map<String, Document> map = doc.asMap();

        Assert.assertEquals(map.get("a").asLong().get(), Long.valueOf(1099511627776l) );
        Assert.assertEquals(map.get("b").asLong().get(), Long.valueOf(1099511627774l) );
        Assert.assertEquals(map.get("c").asLong().get(), Long.valueOf(1099511627779l) );
    }


    @Test
    public void test_private_constructor_for_hyaku_pasento_coverage() throws Exception {
        Constructor constructor = Factories.class.getDeclaredConstructor();
        Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));

        constructor.setAccessible(true);
        constructor.newInstance();
    }
}
